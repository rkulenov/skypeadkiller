# README #

SkypeAdKiller removes Skype ads.
It periodically checks (every 3 sec) if ad appears and removes it.
You can run it once or leave it runnig if you don't like black bar instead of ad.

No installation required. Simply run it.

# DOWNLOAD #

You can download it from here:
[https://bitbucket.org/rkulenov/skypeadkiller/downloads](https://bitbucket.org/rkulenov/skypeadkiller/downloads)

# DESCRIPTION #
[http://blog.avangardo.com/2014/09/skype-ads-killer-2-0/](http://blog.avangardo.com/2014/09/skype-ads-killer-2-0/)